import {
  createComment,
  getComment,
  removeComment,
  updateComment,
} from "../nba/nba.controller";

import { NBA } from "../nba/nba.model";

jest.mock("../nba/nba.model");
const response = {
  status: jest.fn((x) => x),
  json: jest.fn((x) => x),
};

describe("createComment", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("should create a new comment", async () => {
    const request = {
      body: { comment: "Test Comment", stars: 5 },
    };

    NBA.create.mockResolvedValueOnce({ comment: "Test Comment", stars: 5 });

    await createComment(request, response);

    expect(NBA.create).toHaveBeenCalledWith({
      comment: "Test Comment",
      stars: 5,
    });
    expect(response.json).toHaveBeenCalledWith({
      comment: "Test Comment",
      stars: 5,
    });
  });

  it("handle errors when a comment is added", async () => {
    const request = {
      body: { comment: "Test Comment", stars: 5 },
    };
    NBA.create.mockRejectedValueOnce(new Error("Server Error"));

    await createComment(request, response);

    expect(NBA.create).toHaveBeenCalledWith({
      comment: "Test Comment",
      stars: 5,
    });
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ error: "Server Error" });
  });
});

describe("getComment", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("should return all comments", async () => {
    const comments = [
      { comment: "Comment 1", stars: 4 },
      { comment: "Comment 2", stars: 3 },
    ];
    NBA.find.mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(comments),
    });

    await getComment(null, response);

    expect(NBA.find).toHaveBeenCalled();
    expect(response.json).toHaveBeenCalledWith(comments);
  });
});

describe("removeComment", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("should delete a comment", async () => {
    const request = {
      params: { id: "commentId" },
    };

    const comment = { id: "commentId", comment: "Test Comment", stars: 5 };
    NBA.findById.mockResolvedValueOnce({
      exec: jest.fn().mockResolvedValueOnce(comment),
    });
    NBA.deleteOne.mockResolvedValueOnce({ deletedCount: 1 });

    await removeComment(request, response);

    expect(NBA.findById).toHaveBeenCalledWith("commentId");

    expect(response.json).toHaveBeenCalledWith({ deletedCount: 1 });
  });

  it("handle errors when a comment is deleted", async () => {
    const request = {
      params: { id: "commentId" },
    };

    NBA.findById.mockRejectedValueOnce({
      status: 404,
      message: "Comment not found.",
    });

    await removeComment(request, response);

    expect(NBA.findById).toHaveBeenCalledWith("commentId");
    expect(response.status).toHaveBeenCalledWith(404);
    expect(response.json).toHaveBeenCalledWith({
      message: "Comment not found.",
    });
  });
});

describe("updateComment", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should update a comment", async () => {
    const request = {
      params: { id: "commentId" },
      body: { comment: "Updated Comment", stars: 4 },
    };

    const existingComment = {
      _id: "commentId",
      comment: "Original Comment",
      stars: 3,
      save: jest.fn(),
    };

    const mockFindById = jest.spyOn(NBA, "findById");
    mockFindById.mockReturnValueOnce({
      exec: jest.fn().mockResolvedValueOnce(existingComment),
    });

    existingComment.save.mockResolvedValueOnce({
      _id: "commentId",
      comment: "Updated Comment",
      stars: 4,
    });

    await updateComment(request, response);

    expect(mockFindById).toHaveBeenCalledWith("commentId");
    expect(existingComment.save).toHaveBeenCalled();
    expect(response.json).toHaveBeenCalledWith({
      _id: "commentId",
      comment: "Updated Comment",
      stars: 4,
    });
  });

  it("should handle not finding a comment", async () => {
    const request = {
      params: { id: "nonExistentCommentId" },
      body: { comment: "Updated Comment", stars: 4 },
    };

    NBA.findById.mockResolvedValueOnce(null);

    await updateComment(request, response);

    expect(NBA.findById).toHaveBeenCalledWith("nonExistentCommentId");
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({
      message: "Error updating comment.",
    });
  });

  it("should handle errors during update", async () => {
    const request = {
      params: { id: "commentId" },
      body: { comment: "Updated Comment", stars: 4 },
    };

    const existingComment = {
      _id: "commentId",
      comment: "Original Comment",
      stars: 3,
      save: jest.fn(),
    };

    const mockFindById = jest.spyOn(NBA, "findById");
    mockFindById.mockImplementationOnce(() => ({
      exec: jest.fn().mockResolvedValueOnce(existingComment),
    }));

    existingComment.save.mockRejectedValueOnce(new Error("Some error"));

    await updateComment(request, response);

    expect(NBA.findById).toHaveBeenCalledWith("commentId");
    expect(existingComment.save).toHaveBeenCalled();
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({
      message: "Error updating comment.",
    });
  });
});

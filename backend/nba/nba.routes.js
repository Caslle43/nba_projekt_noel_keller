import { Router } from "express";
import {
  getSpieler,
  getTeam,
  getPlayerStatistic,
  createComment,
  getComment,
  removeComment,
  updateComment
} from "./nba.controller.js";

const router = Router();

router.get("/getplayers", getSpieler);
router.get("/getTeams", getTeam);
router.get("/getPlayerStatistics", getPlayerStatistic);
router.post("/createComment", createComment);
router.get("/getComment", getComment);
router.delete("/deleteComment/:id", removeComment);
router.put("/updateComment/:id",updateComment)

export default router;

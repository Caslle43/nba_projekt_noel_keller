import axios from "axios";
import { response } from "express";
import { NBA } from "./nba.model.js";

async function getSpieler(request, response) {
  const teamOptions = {
    method: "GET",
    url: "https://api-nba-v1.p.rapidapi.com/teams",
    headers: {
      "X-RapidAPI-Key": "f3bb3f3c7emsh1e9ac74c85fd859p13d2d6jsn18280c97ea70",
      "X-RapidAPI-Host": "api-nba-v1.p.rapidapi.com",
    },
  };

  try {
    const teamsResponse = await axios.request(teamOptions);
    const teamsData = teamsResponse.data;
    console.log("Teams:", teamsData);

    // Get Players for the first 5 Teams
    const allPlayersData = [];

    for (let i = 0; i < Math.min(3, teamsData.response.length); i++) {
      const team = teamsData.response[i];
      const playerOptions = {
        method: "GET",
        url: "https://api-nba-v1.p.rapidapi.com/players",
        params: {
          team: team.id.toString(),
          season: "2023",
        },
        headers: {
          "X-RapidAPI-Key":
            "f3bb3f3c7emsh1e9ac74c85fd859p13d2d6jsn18280c97ea70",
          "X-RapidAPI-Host": "api-nba-v1.p.rapidapi.com",
        },
      };

      const playersResponse = await axios.request(playerOptions);
      const playersData = playersResponse.data;
      console.log(`Players for ${team.name}:`, playersData);

      allPlayersData.push({ team: team, players: playersData });
    }

    // Send combined data or relevant part to the client
    response.json(allPlayersData);
  } catch (error) {
    console.error(error);
    response.status(500).json({ error: "Internal Server Error" });
  }
}

async function getTeam(request, response) {
  const teamOptions = {
    method: "GET",
    url: "https://api-nba-v1.p.rapidapi.com/teams",
    headers: {
      "X-RapidAPI-Key": "f3bb3f3c7emsh1e9ac74c85fd859p13d2d6jsn18280c97ea70",
      "X-RapidAPI-Host": "api-nba-v1.p.rapidapi.com",
    },
  };
  try {
    console.log("hallo");
    const teamsResponse = await axios.request(teamOptions);
    const teamsData = teamsResponse.data;
    console.log("Teams:", teamsData);
    response.send(teamsData);
  } catch (error) {
    console.error(error);
    response.status(500).json({ error: "Internal Server Error" });
  }
}
//API für die Spielerstatistiken
async function getPlayerStatistic() {
  const axios = require("axios");

  const options = {
    method: "GET",
    url: "https://api-nba-v1.p.rapidapi.com/teams/statistics",
    params: {
      id: "1",
      season: "2023",
    },
    headers: {
      "X-RapidAPI-Key": "f3bb3f3c7emsh1e9ac74c85fd859p13d2d6jsn18280c97ea70",
      "X-RapidAPI-Host": "api-nba-v1.p.rapidapi.com",
    },
  };

  try {
    const response = await axios.request(options);
    console.log(response.data);
  } catch (error) {
    console.error(error);
  }
}
//Neuer kommentar erstellen
async function createComment(req, res) {
  console.log("comment");
  try {
    const newComment = await NBA.create({
      comment: req.body.comment,
      stars: req.body.stars,
    });
    res.json(newComment);
  } catch (error) {
    console.error(error);
    res.status(500);
    res.json({ error: "Server Error" });
  }
}
//Erstellt Kommentar in MongoDB
async function getComment(request, response) {
  await NBA.find()
    .exec()
    .then((comments) => {
      response.json(comments);
    });
}
//Kommentar entfernen
async function removeComment(request, response) {
  const commentId = request.params.id;
  await NBA.findById(commentId)

    .then((comment) => {
      if (!comment) {
        return Promise.reject({
          message: `TODO with id ${commentId} not found.`,
          status: 404,
        });
      }
      return NBA.deleteOne({ _id: commentId });
    })
    .then((deletedComment) => {
      response.json(deletedComment);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}
async function updateComment(request, response) {
  const commentId = request.params.id;

  try {
    const comment = await NBA.findById(commentId).exec();

    if (!comment) {
      return response.status(404);
      response.json({ message: "Comment with id ${commentId} not found." });
    }
    comment.comment = request.body.comment;
    comment.stars = request.body.stars;

    const savedComment = await comment.save();

    response.json(savedComment);
  } catch (error) {
    console.error("Error updating comment:", error);
    response.status(500);
    response.json({ message: "Error updating comment." });
  }
}
export {
  getSpieler,
  getTeam,
  getPlayerStatistic,
  createComment,
  getComment,
  removeComment,
  updateComment,
};

import mongoose from "mongoose";

const nbaComments = new mongoose.Schema({
  comment: {
    type: String,
    required: true,
  },
  stars: {
    type: Number,
    required: true,
  },
});

const NBA = mongoose.model("NBAComments", nbaComments);

export { NBA };

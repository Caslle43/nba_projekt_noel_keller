import express from "express";
import nbaRouter from "./backend/nba/nba.routes.js";
import mongoose from "mongoose";

const app = express();
mongoose.connect("mongodb://localhost:27017/nbaDatabase");
const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.get("/nba", (request, response) => {
  response.send("Hello World");
});

app.use(express.static("frontend"));

app.use(express.json());
app.use("/api/nba", nbaRouter);

mongoose.connection.once("open", () => {
  console.log("Connected to MongoDB");
  app.listen(3001, () => {
    console.log("Server listens to http://localhost:3001");
  });
});

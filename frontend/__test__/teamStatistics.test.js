/**
 * @jest-environment jsdom
 */

import getAllTeamStatistics from "../scripts/teamStatistics";
import axios from "../scripts/libs/axios.min.js";
import Chart from "chart.js/auto";

jest.mock("axios");

describe("Testing the integration of getAllTeamStatistics with Chart.js", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render a Chart.js chart with team statistics", async () => {
    const mockData = [
      { teamId: 1, statistics: { turnovers: 10, points: 100, assists: 20 } },
      { teamId: 2, statistics: { turnovers: 15, points: 120, assists: 25 } },
    ];

    axios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: { response: mockData },
      })
    );

    const season = "2023";
    const data = await getAllTeamStatistics(season);

    expect(Chart).toHaveBeenCalled();
    expect(Chart.mock.instances.length).toBe(1);

    const chartInstance = Chart.mock.instances[0];
    expect(chartInstance.config.type).toBe("bar");
    expect(chartInstance.config.data.labels).toEqual([
      `Team ${mockData[0].teamId}`,
      `Team ${mockData[1].teamId}`,
    ]);

    expect(chartInstance.config.data.datasets.length).toBe(1);
    expect(chartInstance.config.data.datasets[0].data).toEqual([
      mockData[0].statistics.turnovers,
      mockData[1].statistics.turnovers,
    ]);

    expect(axios.get).toHaveBeenCalledWith(
      expect.stringContaining("api-nba-v1.p.rapidapi.com/teams/statistics"),
      expect.objectContaining({
        headers: {
          "X-RapidAPI-Key": expect.any(String),
          "X-RapidAPI-Host": expect.any(String),
        },
      })
    );

    expect(data).toEqual(mockData);
  });
});

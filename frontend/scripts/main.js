//Gibt die Spieler aus
function displayPlayers(players, team) {
  const playersContainer = document.getElementById("players-container");

  if (players) {
    players.forEach((player) => {
      const playerBox = createPlayerBox(player, team);
      playersContainer.appendChild(playerBox);
    });
  } else {
    console.error("Players data is undefined.");
  }
}
//Erstellt für jeden Spieler eine Box
function createPlayerBox(player, team) {
  const playerBox = document.createElement("div");
  playerBox.classList.add("player-box");

  const playerName = document.createElement("h2");
  playerName.textContent = `${player.firstname} ${player.lastname}`;
  playerBox.appendChild(playerName);

  const playerInfo = document.createElement("div");
  playerInfo.classList.add("player-info");

  addAttributeToPlayerBox(playerInfo, "ID", player.id);
  addAttributeToPlayerBox(playerInfo, "Position", player.leagues.standard.pos);
  addAttributeToPlayerBox(playerInfo, "Birthdate", player.birth.date);
  addAttributeToPlayerBox(playerInfo, "Hight", player.height.meters);
  addAttributeToPlayerBox(playerInfo, "Team", team);

  playerBox.appendChild(playerInfo);
  playerBox.addEventListener("mouseenter", function () {
    playerBox.classList.add("hovered");
  });

  playerBox.addEventListener("mouseleave", function () {
    playerBox.classList.remove("hovered");
  });

  return playerBox;
}

function addAttributeToPlayerBox(container, label, value) {
  const attributeElement = document.createElement("p");
  attributeElement.textContent = `${label}: ${value}`;
  container.appendChild(attributeElement);
}
//ruft API controller Funktion auf
function loadPlayers() {
  axios
    .get("http://localhost:3001/api/nba/getplayers")
    .then((res) => {
      console.log(res.data);
      res.data.forEach((team) => {
        const players = team.players.response;
        console.log(players);
        const teamName = team.team.name;
        displayPlayers(players, teamName);
        console.log(players, teamName);
      });
    })
    .catch((err) => {
      console.log(err);
    });
}

loadPlayers();

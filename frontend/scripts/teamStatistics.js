const apiKey = "f3bb3f3c7emsh1e9ac74c85fd859p13d2d6jsn18280c97ea70";
const host = "api-nba-v1.p.rapidapi.com";
//API Funktion um die Teamstatistiken zu laden
const getTeamStatistics = async (teamId, season) => {
  const url = `https://api-nba-v1.p.rapidapi.com/teams/statistics?id=${teamId}&season=${season}`;

  try {
    const response = await axios.get(url, {
      headers: {
        "X-RapidAPI-Key": apiKey,
        "X-RapidAPI-Host": host,
      },
    });

    if (
      response.data &&
      response.data.response &&
      response.data.response.length > 0
    ) {
      return response.data.response.map((statistics) => {
        return {
          turnovers: statistics.turnovers,
          points: statistics.points,
          assists: statistics.assists,
        };
      });
    } else {
      console.error(`Error for team ${teamId}: Invalid response format`);
      console.log(response.data);
      return null;
    }
  } catch (error) {
    console.error(`Error for team ${teamId}:`, error);
    return null;
  }
};
//Holt nur 5 Teamstatistiken aus der API
const getAllTeamStatistics = async (season) => {
  const teamIds = Array.from({ length: 6 }, (_, index) => index + 1);
  const allTeamStatistics = await Promise.all(
    teamIds.map(async (teamId) => {
      const statistics = await getTeamStatistics(teamId, season);
      console.log(statistics);
      return { teamId, statistics };
    })
  );

  return allTeamStatistics;
};
//Erstellen eines Kommentars
const createComment = () => {
  console.log("hallo");
  const commentInput = document.getElementById("commentInput").value;
  const starsinput = document.getElementById("starInput").value;

  const newObject = {
    comment: commentInput,
    stars: starsinput,
  };

  axios
    .post(`http://localhost:3001/api/nba/createComment`, newObject)
    .then((response) => {
      console.log("Comment created:", response.data);
      getComments();
    })
    .catch((error) => {
      console.error("Error creating comment:", error);
    });
};
//Holt alle Kommentare aus der DB
const getComments = () => {
  axios.get(`http://localhost:3001/api/nba/getComment`).then((response) => {
    const comments = response.data;
    displayComments(comments);
  });
};
//Gibt die Kommentare aus
function displayComments(comments) {
  const commentsContainer = document.querySelector(".comments");

  commentsContainer.innerHTML = "";

  comments.forEach((comment) => {
    const commentBox = createCommentBox(comment);
    commentsContainer.appendChild(commentBox);
  });
}
//Erstellt pro Kommentar eine box
function createCommentBox(comment) {
  const commentBox = document.createElement("div");
  commentBox.classList.add("comment-box");

  const commentText = document.createElement("p");
  commentText.textContent = `Kommentar: ${comment.comment}`;
  commentBox.appendChild(commentText);

  const starRating = document.createElement("div");
  starRating.classList.add("star-rating");

  const starsLabel = document.createElement("span");
  starsLabel.textContent = "Sterne: ";
  starRating.appendChild(starsLabel);

  for (let i = 0; i < comment.stars; i++) {
    const star = document.createElement("span");
    star.textContent = "★";
    starRating.appendChild(star);
  }
  const deleteButton = document.createElement("button");
  deleteButton.textContent = "Löschen";
  deleteButton.classList.add("delete-btn");
  deleteButton.addEventListener("click", () => {
    deleteComment(comment._id);
  });
  const updateButton = document.createElement("button");
  updateButton.textContent = "bearbeiten";
  updateButton.classList.add("update-btn");
  updateButton.addEventListener("click", () => {
    updateComment(comment._id);
  });

  commentBox.appendChild(starRating);
  commentBox.appendChild(deleteButton);
  commentBox.appendChild(updateButton);
  return commentBox;
}
//löschen des ausgewählten Kommentars
function deleteComment(commentId) {
  axios
    .delete(`http://localhost:3001/api/nba/deleteComment/${commentId}`)
    .then((response) => {
      console.log("Comment deleted:", response.data);
      getComments();
    })
    .catch((error) => {
      console.error("Error deleting comment:", error);
    });
}
//bearbeiten des ausgewählten Kommentars
function updateComment(commentId) {
  const updatedComment = prompt("Enter the updated comment:");
  const updatedStars = prompt("Enter the updated star rating (1-5):");

  const updatedObject = {
    comment: updatedComment,
    stars: updatedStars,
  };
  axios
    .put(
      `http://localhost:3001/api/nba/updateComment/${commentId}`,
      updatedObject
    )
    .then((response) => {
      console.log("Comment updated:", response.data);
      getComments();
    })
    .catch((error) => {
      console.error("Error updating comment:", error);
    });
}

document
  .getElementById("submitButton")
  .addEventListener("click", createComment);

const season = "2023";

getComments();

getAllTeamStatistics(season)
  .then((data) => {
    const teamNames = [
      "Atlanta Hawks",
      "Boston Celtics",
      "Brisbane Bullets",
      "Brooklyn Nets",
      "Charlotte Hornets",
      "Chicago Bulls",
    ];

    const turnoversData = data.map((team) => team.statistics[0].turnovers);
    const pointsData = data.map((team) => team.statistics[0].points);
    const assistsData = data.map((team) => team.statistics[0].assists);
    //Chart für die Statistiken
    const ctx = document.getElementById("myChart").getContext("2d");
    const myChart = new Chart(ctx, {
      type: "bar",
      data: {
        labels: teamNames,
        datasets: [
          {
            data: turnoversData,
            label: "Turnovers",
            backgroundColor: "rgba(255, 99, 132, 0.6)",
            borderColor: "rgba(255, 99, 132, 1)",
            borderWidth: 1,
          },
          {
            data: pointsData,
            label: "Points",
            backgroundColor: "rgba(54, 162, 235, 0.6)",
            borderColor: "rgba(54, 162, 235, 1)",
            borderWidth: 1,
          },
          {
            data: assistsData,
            label: "Assists",
            backgroundColor: "rgba(75, 192, 192, 0.6)",
            borderColor: "rgba(75, 192, 192, 1)",
            borderWidth: 1,
          },
        ],
      },
      options: {
        responsive: false,
        maintainAspectRatio: false,
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  })
  .catch((error) => {
    console.error("Fehler beim Abrufen der Teamstatistiken:", error);
  });

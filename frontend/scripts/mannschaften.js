//Gibt die Teams aus
function displayTeam(team) {
  const TeamContainer = document.getElementById("team-container");

  if (TeamContainer) {
    if (team) {
      team.forEach((Teams) => {
        const teamBox = createTeamBox(Teams);
        TeamContainer.appendChild(teamBox);
      });
    } else {
      console.error("Players data is undefined.");
    }
  } else {
    console.error("Container element not found.");
  }
}
//erstellt für jedes Team eine box
function createTeamBox(team) {
  const teamBox = document.createElement("div");
  teamBox.classList.add("player-box");

  const teamName = document.createElement("h2");
  teamName.textContent = `${team.name}`;
  teamBox.appendChild(teamName);

  const teaminfo = document.createElement("div");
  teaminfo.classList.add("player-info");
  const logoImg = document.createElement("img");
  logoImg.src = team.logo;
  logoImg.alt = `${team.nickname} Logo`;

  logoImg.classList.add("MannschaftenIMG");
  teaminfo.appendChild(logoImg);

  addAttributeToPlayerBox(teaminfo, "Nickname", team.nickname);
  addAttributeToPlayerBox(teaminfo, "Code", team.code);
  addAttributeToPlayerBox(teaminfo, "City", team.city);

  teamBox.appendChild(teaminfo);

  return teamBox;
}
function addAttributeToPlayerBox(container, label, value) {
  const attributeElement = document.createElement("p");
  attributeElement.textContent = `${label}: ${value}`;
  container.appendChild(attributeElement);
}
//Läd die Teams von der Controller API funktion
function loadTeams() {
  axios
    .get("http://localhost:3001/api/nba/getTeams")
    .then((res) => {
      console.log(res.data);

      const teamlist = res.data.response;
      displayTeam(teamlist);
    })
    .catch((err) => {
      console.log(err);
    });
}
loadTeams();
